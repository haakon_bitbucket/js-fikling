function f1(a,b,c) {
	return a+b+c;
}

function f2(a,b,c) {
	var x=0;
	for (var i=0;i < arguments.length; i++)
		console.log(arguments[i]);
}

function f3(a,b,c='0') {
	var x=0;
	for (var i=0;i < arguments.length; i++)
		console.log(arguments[i]);
}

var myobject = {
	sum2: "",
	increment: function(inc) {
		this.sum2 += typeof inc === 'number' ? inc : 1;
	}
}

myobject.increment('123wdrf');
console.log(myobject.sum2);

console.log(f1(1,2));

f2(1,2,3);

f3(1,2);