﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace AsynchronousJs
{
    public class CalcConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext,
                    Route route,
                    string parameterName,
                    RouteValueDictionary values,
                    RouteDirection routeDirection)
        {
            if (!values.ContainsKey(parameterName))
                return false;

            var calc = (int)values[parameterName];

            return true;
        }
    }
}