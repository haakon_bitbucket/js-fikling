﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AsynchronousJs.Controllers
{
    [RoutePrefix("home")]
    public class HomeController : Controller
    {
        //[Route("")]
        [Route("index")]
        public ActionResult Index()
        {
            return View();
        }

        [Route("about/{msg:alpha}")]
        public ActionResult About(string msg)
        {
            ViewBag.Message = "Your application description page. " + msg;

            return View();
        }

        [Route("~/contact")]
        [Route("contact")]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Route("data")]
        public JsonResult Data()
        {
            var data = Enumerable.Range(0, 100);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [Route("~/double/{num:int}")]
        public ActionResult Double(int num)
        {
            ViewBag.Message = "Doubling: " + num*2;

            return View();
        }
    }
}
